<?php
namespace Xaamin\SimpleExcel;

use Closure;
use DateInterval;
use DateTimeInterface;
use finfo as FileInfo;
use OutOfRangeException;
use InvalidArgumentException;
use OpenSpout\Common\Entity\Row;
use OpenSpout\Common\Entity\Cell;
use OpenSpout\Reader\SheetInterface;
use OpenSpout\Reader\ReaderInterface;
use OpenSpout\Reader\CSV\Sheet as CSVSheet;
use OpenSpout\Reader\ODS\Sheet as ODSSheet;
use OpenSpout\Common\Entity\Cell\FormulaCell;
use OpenSpout\Reader\CSV\Reader as CSVReader;
use OpenSpout\Reader\ODS\Reader as ODSReader;
use OpenSpout\Reader\XLSX\Sheet as XLSXSheet;
use OpenSpout\Reader\CSV\Options as CSVOptions;
use OpenSpout\Reader\ODS\Options as ODSOptions;
use OpenSpout\Reader\XLSX\Reader as XLSXReader;
use OpenSpout\Reader\XLSX\Options as XLSXOptions;
use Xaamin\SimpleExcel\Support\Reader\ReaderFactory;
use OpenSpout\Reader\Exception\ReaderNotOpenedException;
use Xaamin\SimpleExcel\Contracts\ExcelHeaderParserInterface;
use Box\Spout\Reader\IteratorInterface as ReaderIteratorInterface;

class ExcelReader
{
    private string $path;

    /**
     * Sheet instances
     *
     * @var array
     */
    protected $sheets = [];

    /**
     * Excel reader
     *
     * @var CSVReader|XLSXReader|ODSReader
     */
    protected $reader;

    /**
     * Excel header parser
     *
     * @var ExcelHeaderParserInterface
     */
    protected $headerParser;

    /**
     * Row iterator
     *
     * @var ReaderIteratorInterface
     */
    private $rowIterator;

    /**
     * Indicates if the reader needs to compute headers or not
     *
     * @var bool
     */
    protected bool $processHeader = true;

    /**
     * Ignore first row while parsing headers, allows custom headers with files with no headers.
     *
     * @var boolean
     */
    protected bool $ignoreFirstRow = true;

    /**
     * Sheet number to work with
     *
     * @var int
     */
    protected int $sheetNumber = 1;

    /**
     * Iterator offset
     *
     * @var int
     */
    protected int $offset = 0;

    /**
     * Iterator limit
     *
     * @var int
     */
    protected int $limit = -1;

    /**
     * Headers array
     *
     * @var array
     */
    protected array $headers = [];

    /**
     * Config options for reader
     *
     * @var CSVOptions
     */
    protected $options = null;

    /**
     *
     * @param string $file
     * @param CSVOptions|XLSXOptions|ODSOptions|null Config options
     *
     * @return ExcelReader
     */
    public static function create(string $file, $options = null)
    {
        return new static($file);
    }

    /**
     * Constructor
     *
     * @param string|null $path
     * @param CSVOptions|XLSXOptions|ODSOptions|null Config options
     */
    public function __construct(string $path = null, $options = null)
    {
        $this->path = $path;

        if ($path) {
            $this->make($path, $options);
        }
    }

    /**
     * Loads file into excel reader
     *
     * @param string $path
     * @param CSVOptions|XLSXOptions|ODSOptions|null Config options
     *
     * @return ExcelReader
     */
    public function make(string $path, $options = null)
    {
        $mimetype = (new FileInfo())->file($path, FILEINFO_MIME_TYPE);

        if (in_array($mimetype, ['text/plain'])) {
            $this->reader = ReaderFactory::createCSVReader($options ?? $this->options);
        } else {
            $this->reader = ReaderFactory::createFromFile($path, $options ?? $this->options);
        }

        $this->sheets = [];

        return $this;
    }

    public function fresh()
    {
        $this->sheets = [];
        $this->headers = [];
        $this->options = null;
        $this->processHeader = true;
        $this->ignoreFirstRow = true;
        $this->sheetNumber = 1;
        $this->offset = 0;
        $this->limit = -1;

        return $this->make($this->path);
    }

    /**
     * Sets the sheet number to work with
     *
     * @param int $number
     *
     * @throws OutOfRangeException
     *
     * @return ExcelReader
     */
    public function sheet(int $number)
    {
        $this->sheetNumber = $number;

        $sheetInstance = $this->sheets[$this->sheetNumber] ?? null;;

        if (!$sheetInstance) {
            $reader = $this->getReader();

            $reader->getSheetIterator()->rewind();

            foreach ($reader->getSheetIterator() as $key => $sheet) {
                if ($this->sheetNumber != $key) {
                    continue;
                }

                $sheetInstance = $sheet;
            }

            if (!$sheetInstance) {
                throw new OutOfRangeException("Excel file does not contain the sheet number {$number}");
            }

            $this->sheets[$this->sheetNumber] = $sheetInstance;
        }

        return $this;
    }

    /**
     * Get a sheet instance by sheet index
     *
     * @param int $sheetNumber
     *
     * @throws OutOfRangeException
     * @return CSVSheet|XLSXSheet|ODSSheet
     */
    public function getSheet(int $sheetNumber)
    {
        $this->sheet($sheetNumber);

        return $this->sheets[$sheetNumber];
    }

    /**
     * Set LimitIterator Offset.
     *
     * @param int $offset
     *
     * @throws Exception if the offset is lesser than 0
     */
    public function offset(int $offset): self
    {
        if ($offset < 0) {
            throw new InvalidArgumentException(sprintf('%s() expects the offset to be a positive integer or 0, %s given', __METHOD__, $offset));
        }

        $this->offset = $offset;

        return $this;
    }

    /**
     * Set LimitIterator Count.
     *
     * @param int $limit
     *
     * @throws Exception if the limit is lesser than -1
     */
    public function limit(int $limit): self
    {
        if ($limit < -1) {
            throw new InvalidArgumentException(sprintf('%s() expects the limit to be greater or equal to -1, %s given', __METHOD__, $limit));
        }

        $this->limit = $limit;

        return $this;
    }

    /**
     * Get the path for the current working file
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Allows specify if we neeed process headers or not
     *
     * @param boolean $processHeader
     *
     * @return ExcelReader
     */
    public function withHeader(bool $processHeader = true)
    {
        $this->processHeader = $processHeader;

        return $this;
    }

    /**
     * Allows set a headers parser to allow fully customization
     *
     * @param \Xaamin\SimpleExcel\Contracts\ExcelHeaderParserInterface $parser
     * @param bool $ignoreFirstRow
     *
     * @return ExcelReader
     */
    public function withHeaderParser(ExcelHeaderParserInterface $parser, $ignoreFirstRow = true)
    {
        $this->withHeader(true);

        $this->headerParser = $parser;
        $this->ignoreFirstRow = $ignoreFirstRow;

        return $this;
    }

    /**
     * Allows set up a custom delimiter for csv files
     *
     * @param string $delimiter
     *
     * @return ExcelReader
     */
    public function useDelimiter(string $delimiter)
    {
        if (!$this->options) {
            $this->options = new CSVOptions();
        }

        $this->options->FIELD_DELIMITER = $delimiter;

        $this->make($this->path, $this->options);

        return $this;
    }

    /**
     * Allows set up a custom field enclosure character
     *
     * @param string $fieldEnclosure
     *
     * @return ExcelReader
     */
    public function useFieldEnclosure(string $fieldEnclosure)
    {
        if (!$this->options) {
            $this->options = new CSVOptions();
        }

        $this->options->FIELD_ENCLOSURE = $fieldEnclosure;

        $this->make($this->path, $this->options);

        return $this;
    }

    /**
     * Get the inner excel reader
     *
     * @throws ReaderNotOpenedException
     *
     * @return ReaderInterface
     */
    public function getReader(): ReaderInterface
    {
        if (!$this->reader) {
            throw new ReaderNotOpenedException('Reader not opened');
        }

        $this->reader->open($this->path);

        return $this->reader;
    }

    /**
     * Get the file headers
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @return ExcelReader
     */
    public function rewind()
    {
        if ($this->rowIterator) {
            $this->rowIterator->rewind();
        }

        return $this;
    }

    /**
     * Get first row
     *
     * @throws \OpenSpout\Common\Exception\IOException
     * @throws \OpenSpout\Common\Exception\UnsupportedTypeException
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException
     *
     * @return \array
     */
    public function first()
    {
        $value = [];

        $sheet = $this->getSheet($this->sheetNumber);

        $this->rowIterator = $sheet->getRowIterator();

        $this->computeHeaders();

        while ($this->rowIterator->valid()) {
            $row = $this->rowIterator->current();

            $value =  $this->getValueFromRow($row);

            break;
        }

        return !empty($value) ? $value : [];
    }

    /**
     * Rows iterator
     *
     * @throws \OpenSpout\Common\Exception\IOException
     * @throws \OpenSpout\Common\Exception\UnsupportedTypeException
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException
     *
     * @return \Generator|iterable|null
     */
    public function rows()
    {
        $sheet = $this->getSheet($this->sheetNumber, true);

        $this->rowIterator = $sheet->getRowIterator();

        $this->computeHeaders();

        while ($this->rowIterator->valid() && $this->offset > 0 && $this->offset--) {
            $this->rowIterator->next();
        }

        while ($this->rowIterator->valid() && (!$this->limit === -1 || $this->limit--)) {
            $row = $this->rowIterator->current();

            yield $this->getValueFromRow($row);

            $this->rowIterator->next();
        }
    }

    /**
     * Next row iterator
     *
     * @throws \OpenSpout\Common\Exception\IOException
     * @throws \OpenSpout\Common\Exception\UnsupportedTypeException
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException
     *
     * @return array|null
     */
    public function next()
    {
        $sheet = $this->getSheet($this->sheetNumber);

        $this->rowIterator = $sheet->getRowIterator();

        if ($this->processHeader && empty($this->headers)) {
            $this->computeHeaders();
        }

        while ($this->rowIterator->valid() && $this->offset > 0 && $this->offset--) {
            $this->rowIterator->next();
        }

        if ($this->rowIterator->valid() && (!$this->limit === -1 || $this->limit--)) {
            $row = $this->rowIterator->current();

            if (!$row) {
                $this->rowIterator->rewind();

                $row = $this->rowIterator->current();
            }

            $value = $this->getValueFromRow($row);

            $this->rowIterator->next();

            return $value;
        }
    }

    /**
     * Chunk the results of the query.
     *
     * @param   int  $count
     * @param   callable|null $callback
     *
     * @throws \OpenSpout\Common\Exception\IOException
     * @throws \OpenSpout\Common\Exception\UnsupportedTypeException
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException
     *
     * @return \Generator\iterable|null
     */
    public function chunk($count, callable $callback = null)
    {
        $sheet = $this->getSheet($this->sheetNumber, true);

        $this->rowIterator = $sheet->getRowIterator();

        $this->computeHeaders();

        $offset = $this->offset;
        $limit = $count;
        $collection = [];

        while ($this->rowIterator->valid() && $this->offset > 0 && $this->offset--) {
            $this->rowIterator->next();
        }

        $currentRow = $offset + 1;

        while ($this->rowIterator->valid()) {
            $row = $this->rowIterator->current();

            $collection[] = $this->getValueFromRow($row);

            $this->rowIterator->next();

            $currentRow++;

            if ($currentRow <= $offset + $limit) {
                continue;
            }

            if ($callback && $callback instanceof Closure) {
                $callback($collection);
            } else if ($callback){
                call_user_func_array($callback, $collection);
            } else {
                yield $collection;
            }

            $offset = $offset + $limit;

            $collection = [];
        }

        if (!empty($collection)) {
            if ($callback && $callback instanceof Closure) {
                $callback($collection);
            } else if ($callback){
                call_user_func_array($callback, $collection);
            } else {
                yield $collection;
            }
        }
    }

    /**
     * @param string $path
     * @param callable|null $callback
     *
     * @throws \OpenSpout\Common\Exception\IOException
     * @throws \OpenSpout\Common\Exception\UnsupportedTypeException
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException
     *
     * @return array
     */
    public function import(callable $callback = null)
    {
        $sheet = $this->getSheet($this->sheetNumber);

        $collection = $this->importSheet($sheet, $callback);

        return $collection;
    }

    /**
     * @param SheetInterface $sheet
     * @param callable|null $callback
     *
     * @throws \OpenSpout\Common\Exception\IOException
     * @throws \OpenSpout\Common\Exception\UnsupportedTypeException
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException
     *
     * @return array
     */
    private function importSheet(SheetInterface $sheet, callable $callback = null)
    {
        $collection = [];

        foreach ($sheet->getRowIterator() as $row) {
            $this->rowIterator = $sheet->getRowIterator();

            $this->computeHeaders();

            $values = $this->getValueFromRow($row);

            if ($callback) {
                $result = $callback($values);

                if (!!$values) {
                    $collection[] = $result;
                }
            } else {
                $collection[] = $values;
            }
        }

        return $collection;
    }

    /**
     * @param string $path
     * @param callable|null $callback
     *
     * @throws \OpenSpout\Common\Exception\IOException
     * @throws \OpenSpout\Common\Exception\UnsupportedTypeException
     * @throws \OpenSpout\Reader\Exception\ReaderNotOpenedException
     *
     * @return array
     */
    public function importSheets(callable $callback = null)
    {
        $reader = $this->getReader();
        $reader->getSheetIterator()->rewind();

        $collections = [];

        foreach ($reader->getSheetIterator() as $sheet) {
            $collections[] = $this->importSheet($sheet, $callback);
        }

        return $collections;
    }

    /**
     * Computes the headers for the current file
     *
     * @return ExcelReader
     */
    protected function computeHeaders()
    {
        $this->rowIterator->rewind();

        /** @var \OpenSpout\Common\Entity\Row $firstRow */
        $firstRow = $this->rowIterator->current();

        if (is_null($firstRow)) {
            $this->withHeader(false);
        }

        if ($this->processHeader) {
            $headers = $firstRow->toArray();

            if (!!$this->headerParser) {
                $this->headerParser->reset();

                $this->headers = $this->headerParser->getColumns($headers);
            } else {
                $this->headers = $headers;
            }

            !!$this->ignoreFirstRow && $this->rowIterator->next();
        }

        return $this;
    }

    /**
     * Get row values
     *
     * @param \OpenSpout\Common\Entity\Row $row
     *
     * @return array
     */
    protected function getValueFromRow(Row $row): array
    {
        /** @var array */
        $values = $this->rowToArray($row);

        if (!$this->processHeader) {
            return $values;
        }

        $values = array_slice($values, 0, count($this->headers));

        while (count($values) < count($this->headers)) {
            $values[] = '';
        }

        return array_combine($this->headers, $values);
    }

    /**
     * @param \OpenSpout\Common\Entity\Row $row
     *
     * @return list<null|bool|DateInterval|DateTimeInterface|float|int|string> The row values, as array
     */
    public function rowToArray(Row $row): array
    {
        return array_map(static function (Cell $cell): null|bool|DateInterval|DateTimeInterface|float|int|string {
            return $cell instanceof FormulaCell
                ? $cell->getComputedValue()
                : $cell->getValue();
        }, $row->getCells());
    }

    /**
     * Close the excel reader
     *
     * @return void
     */
    public function close()
    {
        $this->reader->close();
    }

    /**
     * Class destruct
     */
    public function __destruct()
    {
        $this->close();
    }
}
