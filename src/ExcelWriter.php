<?php
namespace Xaamin\SimpleExcel;

use OpenSpout\Writer\WriterInterface;
use OpenSpout\Common\Entity\Style\Style;
use OpenSpout\Writer\Common\Entity\Sheet;
use OpenSpout\Writer\CSV\Writer as CSVWriter;
use OpenSpout\Writer\ODS\Writer as ODSWriter;
use OpenSpout\Writer\CSV\Options as CSVOptions;
use OpenSpout\Writer\ODS\Options as ODSOptions;
use OpenSpout\Writer\XLSX\Writer as XLSXWriter;
use OpenSpout\Writer\XLSX\Options as XLSXOptions;
use Xaamin\SimpleExcel\Support\Writer\WriterFactory;
use Xaamin\SimpleExcel\Support\Writer\WriterEntityFactory;

class ExcelWriter
{
    /**
     * Excel writer implementation
     *
     * @var CSVWriter|XLSXWriter|ODSWriter
     */
    protected $writer;

    /**
     * File path
     *
     * @var string
     */
    protected string $path = '';

    /**
     * Indicates if the reader needs to compute headers or not
     *
     * @var bool
     */
    protected bool $processHeader = true;

    /**
     * Flag to control when the headers was computed
     *
     * @var bool
     */
    protected bool $headerHasBeenWritten = false;

    /**
     * Total of rows processing counter
     *
     * @var int
     */
    protected int $numberOfRows = 0;

    /**
     * Indicates if the writer is a stream download
     *
     * @var bool
     */
    protected bool $streamDownload = false;

    /**
     * Writer options
     *
     * @var CSVOptions
     */
    protected $options;

    /**
     * Constructor
     *
     * @param string|null $path
     * @param boolean $streamDownload
     * @param CSVOptions|XLSXOptions|ODSOptions|null Config options
     */
    protected function __construct(string $path = null, $streamDownload = false, $options = null)
    {
        $this->path = $path;

        if (!!$path) {
            $this->make($path, $streamDownload, $options);
        }

        $this->options = $options;
    }

    /**
     * Create a new instance of excel writer
     *
     * @param string $file
     * @param boolean $streamDownload
     * @param CSVOptions|XLSXOptions|ODSOptions|null Config options
     *
     * @return ExcelWriter
     */
    public static function create(string $file, $streamDownload = false, $options = null)
    {
        return new static($file, $streamDownload, $options);
    }

    /**
     * Creates an excel writer ready to work with files or stream downloads
     *
     * @param string $path
     * @param boolean $streamDownload
     * @param CSVOptions|XLSXOptions|ODSOptions|null Config options
     *
     * @return ExcelWriter
     */
    public function make(string $path, $streamDownload = false, $options = null)
    {
        $this->writer = WriterFactory::createFromFile($path, $options ?? $this->options);

        $this->streamDownload = $streamDownload;

        if ($this->streamDownload) {
            $this->writer->openToBrowser($this->path);
        } else {
            $this->writer->openToFile($this->path);
        }

        return $this;
    }

    /**
     * Creates an excel writer as stream download
     *
     * @param string $downloadName
     * @return ExcelWriter
     */
    public static function downloadAs(string $downloadName)
    {
        return new static($downloadName, true);
    }

    /**
     * Current file path
     *
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Get the inner writer
     *
     * @return WriterInterface
     */
    public function getWriter(): WriterInterface
    {
        return $this->writer;
    }

    /**
     * Returns the rows processed count
     *
     * @return integer
     */
    public function getNumberOfRows(): int
    {
        return $this->numberOfRows;
    }

    /**
     * Set up if the writer needs to process the headers or not
     *
     * @param boolean $processHeader
     * @return ExcelWriter
     */
    public function withHeader(bool $processHeader = true)
    {
        $this->processHeader = $processHeader;

        return $this;
    }

    /**
     * @param \OpenSpout\Common\Entity\Row|array $row
     * @param \OpenSpout\Common\Entity\Style\Style|null $style
     */
    public function addRow($row, Style $style = null)
    {
        if (is_array($row)) {
            if ($this->processHeader && !$this->headerHasBeenWritten) {
                $this->writeHeader($row);
            }

            $row = WriterEntityFactory::createRowFromArray($row, $style);
        }

        $this->writer->addRow($row);
        $this->numberOfRows++;

        return $this;
    }

    /**
     * Adds multiple rows
     *
     * @param iterable $rows
     * @return ExcelWriter
     */
    public function addRows(iterable $rows)
    {
        foreach ($rows as $row) {
            $this->addRow($row);
        }

        return $this;
    }

    /**
     * Allows set up a custom delimiter for csv files
     *
     * @param string $delimiter
     *
     * @return ExcelWriter
     */
    public function useDelimiter(string $delimiter)
    {
        if (!$this->options) {
            $this->options = new CSVOptions();
        }

        $this->options->FIELD_DELIMITER = $delimiter;

        $this->make($this->path, $this->streamDownload, $this->options);

        return $this;
    }

    /**
     * Allows set up a custom field enclosure character
     *
     * @param string $fieldEnclosure
     *
     * @return ExcelWriter
     */
    public function useFieldEnclosure(string $fieldEnclosure)
    {
        if (!$this->options) {
            $this->options = new CSVOptions();
        }

        $this->options->FIELD_ENCLOSURE = $fieldEnclosure;

        $this->make($this->path, $this->streamDownload, $this->options);

        return $this;
    }

    /**
     * Closes the writer and send it to the browser
     *
     * @return void
     */
    public function sendToBrowser()
    {
        if (!$this->streamDownload) {
            $this->writer->close();

            exit();
        }
    }

    /**
     * Write headers
     *
     * @param array $row
     * @return void
     */
    protected function writeHeader(array $row)
    {
        $headerValues = array_keys($row);

        $headerRow = WriterEntityFactory::createRowFromArray($headerValues);

        $this->writer->addRow($headerRow);
        $this->numberOfRows++;

        $this->headerHasBeenWritten = true;
    }

    /**
     * Sets the active sheet name
     *
     * @param string $name
     *
     * @return ExcelWriter
     */
    public function setName($name)
    {
        $this->writer->getCurrentSheet()->setName($name);

        return $this;
    }

    /**
     * Get the active sheet
     *
     * @return Sheet
     */
    public function getCurrentSheet()
    {
        return $this->writer->getCurrentSheet();
    }

    /**
     * Adds a new sheet and make it active
     *
     * @param string|null $name
     *
     * @return ExcelWriter
     */
    public function addNewSheet($name = null)
    {
        $newSheet = $this->writer->addNewSheetAndMakeItCurrent();

        if ($name) {
            $newSheet->setName($name);
        }

        if ($this->processHeader) {
            $this->headerHasBeenWritten = false;
        }

        return $this;
    }

    /**
     * Set active sheet by name or Sheet instance
     *
     * @param Sheet|string $sheet
     *
     * @return ExcelWriter
     */
    public function setActiveSheet($sheet)
    {
        if ($sheet instanceof Sheet) {
            $this->writer->setCurrentSheet($sheet);
        } else {
            foreach ($this->writer->getSheets() as $current) {
                if ($current->getName() === $sheet) {
                    $this->writer->setCurrentSheet($current);

                    break;
                }
            }
        }

        return $this;
    }

    public function close()
    {
        $this->writer->close();
    }

    public function __destruct()
    {
        $this->close();
    }
}
