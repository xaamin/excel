<?php
namespace Xaamin\SimpleExcel\Support\Writer;

use Xaamin\SimpleExcel\Support\Type;
use OpenSpout\Writer\WriterInterface;
use OpenSpout\Writer\CSV\Writer as CSVWriter;
use OpenSpout\Writer\ODS\Writer as ODSWriter;
use OpenSpout\Writer\CSV\Options as CSVOptions;
use OpenSpout\Writer\ODS\Options as ODSOptions;
use OpenSpout\Writer\XLSX\Writer as XLSXWriter;
use OpenSpout\Writer\XLSX\Options as XLSXOptions;
use OpenSpout\Common\Exception\UnsupportedTypeException;

/**
 * Class WriterFactory
 * This factory is used to create writers, based on the type of the file to be read.
 * It supports CSV, XLSX and ODS formats.
 */
class WriterFactory
{
    /**
     * This creates an instance of the appropriate writer, given the extension of the file to be written
     *
     * @param string $path The path to the spreadsheet file. Supported extensions are .csv,.ods and .xlsx
     * @param CSVOptions|XLSXOptions|ODSOptions|null Config options
     *
     * @throws UnsupportedTypeException
     * @return WriterInterface
     */
    public static function createFromFile(string $path, $options = null)
    {
        $extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));

        return self::createFromType($extension, $options);
    }

    /**
     * This creates an instance of the appropriate writer, given the type of the file to be written
     *
     * @param string $writerType Type of the writer to instantiate
     * @param CSVOptions|XLSXOptions|ODSOptions|null Config options
     * @throws UnsupportedTypeException
     *
     * @return WriterInterface
     */
    public static function createFromType($writerType, $options = null)
    {
        switch ($writerType) {
            case Type::CSV:
                return self::createCSVWriter($options);
            case Type::XLSX:
                return self::createXLSXWriter($options);
            case Type::ODS:
                return self::createODSWriter($options);
            default:
                throw new UnsupportedTypeException('No writers supporting the given type: ' . $writerType);
        }
    }

    /**
     * @param CSVOptions|null Config options
     *
     * @return CSVWriter
     */
    public static function createCSVWriter(CSVOptions $options = null)
    {
        return new CSVWriter($options);
    }

    /**
     * @param XLSXOptions|null Config options
     *
     * @return XLSXWriter
     */
    public static function createXLSXWriter(XLSXOptions $options = null)
    {
        return new XLSXWriter($options);
    }

    /**
     * @param ODSOptions|null Config options
     *
     * @return ODSWriter
     */
    public static function createODSWriter(ODSOptions $options = null)
    {
        return new ODSWriter($options);
    }
}
