<?php
namespace Xaamin\SimpleExcel\Support\Reader;

use Xaamin\SimpleExcel\Support\Type;
use OpenSpout\Reader\ReaderInterface;
use OpenSpout\Reader\CSV\Reader as CSVReader;
use OpenSpout\Reader\ODS\Reader as ODSReader;
use OpenSpout\Reader\CSV\Options as CSVOptions;
use OpenSpout\Reader\ODS\Options as ODSOptions;
use OpenSpout\Reader\XLSX\Reader as XLSXReader;
use OpenSpout\Reader\XLSX\Options as XLSXOptions;
use OpenSpout\Common\Exception\UnsupportedTypeException;

/**
 * Class ReaderFactory
 * This factory is used to create readers, based on the type of the file to be read.
 * It supports CSV, XLSX and ODS formats.
 */
class ReaderFactory
{
    /**
     * Creates a reader by file extension
     *
     * @param string $path The path to the spreadsheet file. Supported extensions are .csv,.ods and .xlsx
     * @param CSVOptions|ODSOptions|XLSXOptions|null Config options
     *
     * @throws UnsupportedTypeException
     * @return ReaderInterface
     */
    public static function createFromFile(string $path, $options = null)
    {
        $extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));

        return self::createFromType($extension, $options);
    }

    /**
     * This creates an instance of the appropriate reader, given the type of the file to be read
     *
     * @param  string $readerType Type of the reader to instantiate
     * @param CSVOptions|ODSOptions|XLSXOptions|null Config options
     *
     * @throws UnsupportedTypeException
     * @return ReaderInterface
     */
    public static function createFromType($readerType, $options = null)
    {
        switch ($readerType) {
            case Type::CSV:
                return self::createCSVReader($options);
            case Type::XLSX:
                return self::createXLSXReader($options);
            case Type::ODS:
                return self::createODSReader($options);
            default:
                throw new UnsupportedTypeException('No readers supporting the given type: ' . $readerType);
        }
    }

    /**
     * @param CSVOptions|null Config options
     *
     * @return CSVReader
     */
    public static function createCSVReader(CSVOptions $options = null)
    {
        return new CSVReader($options);
    }

    /**
     * @param XLSXOptions|null Config options
     *
     * @return XLSXReader
     */
    public static function createXLSXReader(XLSXOptions $options = null)
    {
        return new XLSXReader($options);
    }

    /**
     * @param ODSOptions|null Config options
     *
     * @return ODSReader
     */
    public static function createODSReader(ODSOptions $options = null)
    {
        return new ODSReader($options);
    }
}
