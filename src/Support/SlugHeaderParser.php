<?php
namespace Xaamin\SimpleExcel\Support;

use Xaamin\SimpleExcel\Contracts\ExcelHeaderParserInterface;

class SlugHeaderParser implements ExcelHeaderParserInterface
{
    /**
     * Array of column indexes
     *
     * @var array
     */
    protected $names = [];

    public function getColumns(array $cells)
    {
        return array_map([$this, 'getIndex'], $cells);
    }

    public function reset()
    {
        $this->names = [];
    }

    /**
     * Get index name
     * @param  string $cell Cell name
     * @return string
     */
    protected function getIndex($cell)
    {
        $index = $this->getSluggedIndex($cell);

        if ($index === '') {
            $index = '__empty_header';
        }

        if (in_array($index, $this->names)) {
            $index = $this->appendOrIncreaseStringCount($index);
        }

        if (!in_array($index, $this->names)) {
            $this->names[] = $index;
        }

        return $index;
    }

    /**
     * Get slugged index
     * @param  string $value
     * @param bool    $ascii
     * @return string
     */
    protected function getSluggedIndex($value)
    {
        $index = str_camel_to_title_case($value);
        $index = str_to_title_case(preg_replace('/\.+/', ' ', $index));
        $index = str_replace_diacritics($index);
        $index = preg_replace('/\s+/', ' ', $index);
        $index = str_to_slug_case(str_to_lower_case($index), '_');

        return trim($index);
    }

    /**
     * Append or increase the count at the String like: test to test_1
     * @param string $index
     * @return string
     */
    protected function appendOrIncreaseStringCount($index)
    {
        do {
            if (preg_match("/(\d+)$/", $index, $matches)) {
                // increase +1
                $index = preg_replace_callback( "/(\d+)$/",
                    function ($matches) {
                        return ++$matches[1];
                    }, $index);
            } else {
                $index .= '_1';
            }

        } while(in_array($index, $this->names));

        return $index;
    }

}
