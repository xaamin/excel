<?php
namespace Xaamin\SimpleExcel\Support;

/**
 * Class Type
 * This class references the supported types
 */
abstract class Type
{
    const CSV = 'csv';
    const XLSX = 'xlsx';
    const ODS = 'ods';
}
