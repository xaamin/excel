<?php
namespace Xaamin\SimpleExcel\Contracts;

interface ExcelHeaderParserInterface
{
    /**
     * Get cells names
     * @param  array $cells Cells array
     * @return array
     */
    public function getColumns(array $cells);

    /**
     * Reset previous cell parsing allowing reuse the same reader instances
     *
     * @return void
     */
    public function reset();
}